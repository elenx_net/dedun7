import logging
from multiprocessing import cpu_count

import pandas as pd
from pandarallel import pandarallel


def configure():
    logging.basicConfig(level=logging.INFO)

    # pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', 2000)
    pd.set_option('display.float_format', '{:20,.2f}'.format)
    pd.set_option('display.max_colwidth', None)

    pandarallel.initialize(nb_workers=2 * cpu_count(), verbose=1, progress_bar=True)
