import financedatabase as fd
import pandas as pd

from dedun.provider.data_frame_provider import DataFrameProvider


class BasicDataFrameProvider(DataFrameProvider):
    def __init__(self):
        super().__init__('1_basic_data_frame')

    def create(self) -> pd.DataFrame:
        funds = fd.search_products(fd.select_etfs(), '')

        df = pd.DataFrame(funds).transpose()

        df = df[df['currency'].isin({'USD', 'EUR', 'PLN', 'CHF', 'GBP'})]

        df = df[~df['summary'].str.contains('[Ii]nverse')]
        df = df[~df['summary'].str.contains('[Ss]hort')]
        df = df[~df['summary'].str.contains('-[0-9]+[xX]')]

        df['symbols'] = df.index.to_series()

        return df
