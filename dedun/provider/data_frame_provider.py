import os
from typing import Optional

import pandas as pd


class DataFrameProvider:
    def __init__(self, name: str, cache_directory: str = 'cache/data_frames/'):
        self.name = name
        self.cache_directory = cache_directory

    def provide(self) -> pd.DataFrame:
        cached_data_frame: Optional[pd.DataFrame] = self.try_load()

        if cached_data_frame is None:
            df: pd.DataFrame = self.create()
            self.save(df)
        else:
            return cached_data_frame

    def try_load(self) -> Optional[pd.DataFrame]:
        if os.path.isfile(self.cache_directory + self.name + '.pickle'):
            return pd.read_pickle(self.cache_directory + self.name + '.pickle')

    def save(self, df: pd.DataFrame):
        df.to_pickle(self.cache_directory + self.name + '.pickle')
        df.to_csv(self.cache_directory + self.name + '.csv')

    def create(self) -> pd.DataFrame:
        pass
