import logging
from functools import reduce
from typing import Any, List, Dict, Iterator, Set

import pandas as pd

from dedun.provider.data_frame_provider import DataFrameProvider
from dedun.provider.fundamental.fundamental_data_frame_provider import FundamentalDataFrameProvider


class CleanDataFrameProvider(DataFrameProvider):
    def __init__(self, fundamental_data_frame_provider: FundamentalDataFrameProvider):
        super().__init__('3_clean_data_frame')
        self.fundamental_data_frame_provider = fundamental_data_frame_provider

    def create(self) -> pd.DataFrame:
        df: pd.DataFrame = self.fundamental_data_frame_provider.provide()

        df: pd.DataFrame = df.dropna(how='all')
        df: pd.DataFrame = self.drop_singleton_columns(df)
        df: pd.DataFrame = self.unpack_lists_and_sets(df)
        df: pd.DataFrame = self.merge_identical_columns(df)

        return df

    def drop_singleton_columns(self, df: pd.DataFrame) -> pd.DataFrame:
        for column in list(df.columns.values):
            values = df[column].squeeze().to_list()

            try:
                if len(set(values)) == 1 or len(column.unique()) == 1 or reduce(lambda first, second: str(first) == str(second), values):
                    df = df.drop(columns=[column])
                    logging.info('Dropped column ' + column)
            except:
                pass

        return df

    def unpack_lists_and_sets(self, df: pd.DataFrame) -> pd.DataFrame:
        for column in list(df.columns.values):
            values = df[column].squeeze().to_list()

            should_unpack_list: Iterator[bool] = map(lambda it: self.is_unpackable(it), values)

            if all(should_unpack_list):
                df: pd.DataFrame = df[column].applymap(lambda it: None if len(it) == 0 else list(it)[0])

        return df

    def is_unpackable(self, it: Any) -> bool:
        if it is None:
            return True
        elif (isinstance(it, List) or isinstance(it, Set)) and len(it) <= 1:
            return True
        else:
            return False


    def merge_identical_columns(self, df: pd.DataFrame) -> pd.DataFrame:
        columns_hashes: Dict[int, List[str]] = {}

        for column in list(df.columns.values):
            try:
                series_hashes: pd.Series = pd.util.hash_pandas_object(df[column])
                column_hash: int = reduce(lambda first, second: first * second, series_hashes)

                if columns_hashes.__contains__(column_hash):
                    columns_hashes[column_hash].append(column)
                else:
                    columns_hashes[column_hash] = [column]
            except:
                pass

        for _, column_names in columns_hashes.items():
            if len(column_names) > 1:
                final_column_name: str = reduce(lambda first, second: first + ' & ' + second, column_names)
                df = df.rename(columns={column_names[0]: final_column_name})
                df = df.drop(columns=column_names[1:])
                logging.info('Merged columns to ' + final_column_name)

        return df
