from typing import Any, Dict, Union, Set, List

import pandas as pd

from dedun.provider.basic_data_frame_provider import BasicDataFrameProvider
from dedun.provider.data_frame_provider import DataFrameProvider
from dedun.provider.fundamental.yahoo_fundamental_service import YahooFundamentalService


class FundamentalDataFrameProvider(DataFrameProvider):
    def __init__(self, basic_data_frame_provider: BasicDataFrameProvider):
        super().__init__('2_fundamental_data_frame')
        self.basic_data_frame_provider = basic_data_frame_provider

    def create(self) -> pd.DataFrame:
        basic_data_frame: pd.DataFrame = self.basic_data_frame_provider.provide()

        symbols: pd.Series = basic_data_frame.index.to_series()
        yahoo_fundamentals_provider: YahooFundamentalService = YahooFundamentalService()

        fundamental_data_frame: pd.DataFrame = symbols.apply(lambda row: yahoo_fundamentals_provider.fetch(row))
        fundamental_data_frame: pd.DataFrame = fundamental_data_frame.apply(lambda row: self.tree_to_columns(row))
        fundamental_data_frame: pd.DataFrame = pd.DataFrame(data=list(fundamental_data_frame), index=symbols)

        joined_data_frame: pd.DataFrame = basic_data_frame.join(fundamental_data_frame, how='inner')

        return joined_data_frame

    def tree_to_columns(self, dictionary: Dict[str, Any], current_path: str = '') -> Dict[str, Any]:
        result: Dict[str, Any] = {}

        if current_path != '':
            current_path = current_path + '_'

        for key, value in dictionary.items():
            if isinstance(value, dict):
                result.update(self.tree_to_columns(value, current_path + key))
            elif isinstance(value, list) and len(value) > 0 and len(value) != len(value[0].keys()) and isinstance(value[0], dict):
                for element in value:
                    result.update(self.tree_to_columns(element, current_path + key))
            else:
                result[current_path + key] = value

        return result
