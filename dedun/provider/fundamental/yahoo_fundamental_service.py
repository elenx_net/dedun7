import logging
import os
import pickle
from typing import Dict, Any

from yfinance.utils import get_json


class YahooFundamentalService:
    def __init__(self, cache_directory: str = 'cache/fundamental/'):
        self.cache_directory: str = cache_directory

    def fetch(self, symbol: str) -> Dict[str, Any]:
        if not os.path.isdir(self.cache_directory):
            os.remove(self.cache_directory)
            os.mkdir(self.cache_directory)

        cache_file = self.cache_directory + symbol

        if os.path.isfile(cache_file):
            with open(cache_file, 'rb') as file:
                data: Dict[str, Any] = pickle.load(file)

            logging.debug('Loaded from cache ' + symbol)

            return data
        else:
            data: Dict[str, Any] = get_json("https://finance.yahoo.com/quote/" + symbol)

            with open(cache_file, 'wb') as file:
                pickle.dump(data, file)

            logging.info('Fetched and cached ' + symbol)

            return data
