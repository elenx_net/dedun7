import pandas as pd

from dedun.provider.data_frame_provider import DataFrameProvider
from dedun.provider.fundamental.clean_data_frame_provider import CleanDataFrameProvider


class FilterDataFrameProvider(DataFrameProvider):
    def __init__(self, clean_data_frame_provider: CleanDataFrameProvider):
        super().__init__('5_filter_data_frame')
        self.clean_data_frame_provider = clean_data_frame_provider

    def create(self) -> pd.DataFrame:
        df: pd.DataFrame = self.clean_data_frame_provider.provide()

        df = df[df['topHoldings_bondPosition'] < 0.05]
        df = df[df['price_quoteType & quoteType_quoteType'] == 'ETF']

        df = df.drop(columns=[
            'price_quoteType & quoteType_quoteType',
            'fundProfile_styleBoxUrl',
            'family',
            # 'defaultKeyStatistics_fundFamily & fundProfile_family',
            # 'defaultKeyStatistics_priceHint & summaryDetail_priceHint',
            # 'defaultKeyStatistics_maxAge & fundProfile_maxAge & topHoldings_maxAge & fundPerformance_maxAge & summaryDetail_maxAge',
            # 'price_quoteSourceName',
            # 'price_regularMarketTime',
            # 'price_currencySymbol',
            # # 'price_marketPrePrice',
            # 'price_exchangeDataDelayedBy',
            # 'price_postMarketChange',
            # 'price_postMarketPrice',
            # 'price_priceHint',
            # 'price_regularMarketSource',
            # 'price_marketState',
            # 'assetProfile_maxAge',
            # 'assetProfile_phone',
            # # 'assetPrice_preMarketSource',
            # 'pageViews_maxAge',
            # 'price_preMarketTime',
            # 'price_preMarketChangePercent',
            # 'financialsTemplate_maxAge',
            # 'price_postMarketChangePercent',
            # 'price_quoteType & quoteType_quoteType'
        ])

        return df
