from typing import List

import pandas as pd

from dedun.provider.basic_data_frame_provider import BasicDataFrameProvider
from dedun.provider.data_frame_provider import DataFrameProvider
from dedun.provider.technical.yahoo_technical_service import YahooTechnicalService


class TechnicalDataFrameProvider(DataFrameProvider):
    def __init__(self, basic_data_frame_provider: BasicDataFrameProvider):
        super().__init__('4_technical_data_frame')
        self.basic_data_frame_provider = basic_data_frame_provider

    def create(self) -> pd.DataFrame:
        df: pd.DataFrame = self.basic_data_frame_provider.provide()
        symbols: List[str] = df['symbols'].squeeze().to_list()

        portfolio: pd.DataFrame = YahooTechnicalService().fetch(symbols)

        return portfolio
