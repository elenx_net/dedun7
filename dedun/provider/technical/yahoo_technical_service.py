import logging
import os
import pickle
from typing import List

import pandas as pd
import yfinance as yf


class YahooTechnicalService:
    def __init__(self, cache_directory: str = 'cache/technical/'):
        self.cache_directory = cache_directory

    def fetch(self, symbols: List[str]) -> pd.DataFrame:
        if not os.path.isdir(self.cache_directory):
            os.remove(self.cache_directory)
            os.mkdir(self.cache_directory)

        data_frames: List[pd.DataFrame] = []

        for symbol in symbols:
            df: pd.DataFrame = self.fetch_single(symbol)
            data_frames.append(df)

        result_data_frame: pd.DataFrame = pd.concat(data_frames, axis=1, keys=symbols)
        result_data_frame.dropna(inplace=True, how='all', axis=0)
        result_data_frame.dropna(inplace=True, how='all', axis=1)

        return result_data_frame

    def fetch_single(self, symbol: str) -> pd.DataFrame:
        cache_file = self.cache_directory + symbol

        if os.path.isfile(cache_file):
            with open(cache_file, 'rb') as file:
                data = pickle.load(file)

            logging.debug('Loaded from cache ' + symbol)

            return data
        else:
            data = yf.download(symbol, auto_adjust=True, actions="inline", progress=False)

            with open(cache_file, 'wb') as file:
                pickle.dump(data, file)

            logging.info('Fetched and cached ' + symbol)

            return data
