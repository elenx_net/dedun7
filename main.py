import asyncio
import os
from concurrent.futures.thread import ThreadPoolExecutor

import pandas as pd

from dedun.configuration.configuration import configure
from dedun.configuration.dependency_injection_container import filter_data_frame_provider

configure()


###

async def main():
    filter_data_frame: pd.DataFrame = filter_data_frame_provider.provide()

    print()


###

loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)
loop.set_debug(True)
loop.set_default_executor(ThreadPoolExecutor(os.cpu_count() * 2))
loop.run_until_complete(main())
