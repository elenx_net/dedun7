from datetime import datetime
from multiprocessing import cpu_count

import pandas as pd
from pandarallel import pandarallel
from wallstreet import Call

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', 2000)
pd.set_option('display.float_format', '{:20,.2f}'.format)
pd.set_option('display.max_colwidth', None)

pandarallel.initialize(nb_workers=2 * cpu_count(), verbose=1, progress_bar=True)


def options_of(symbol: str):
    call = Call(symbol)
    soonest_expiration_as_string = call.expirations[0]

    soonest_expiration_as_date = datetime.strptime(soonest_expiration_as_string, '%d-%m-%Y')

    day = soonest_expiration_as_date.day
    month = soonest_expiration_as_date.month
    year = soonest_expiration_as_date.year

    strikes = Call(symbol, d=day, m=month, y=year).strikes

    df = pd.DataFrame({'strike': strikes})

    df['calls'] = df.parallel_apply(lambda row: Call(symbol, d=day, m=month, y=year, strike=row['strike']), axis=1)

    return df


print(options_of('TQQQ'))
