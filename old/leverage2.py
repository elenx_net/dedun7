import numpy as np
import plotly
import plotly.graph_objects as graph




def daily_rebalancing_loss(standard_deviation: float) -> float:
    return np.sqrt(1 - standard_deviation * standard_deviation) - 1


def daily_rebalanced_expected_return(expected_return: float, standard_deviation: float) -> float:
    return expected_return * (1 + daily_rebalancing_loss(standard_deviation))


one_dimensional_space = np.linspace(0, 1, 100)

expected_returns, standard_deviations = np.meshgrid(one_dimensional_space, one_dimensional_space)
daily_rebalanced_expected_returns = daily_rebalanced_expected_return(expected_returns, standard_deviations)

fig = graph.Figure(data=graph.Surface(z=daily_rebalanced_expected_returns, showscale=False))
fig.update_layout(
    title='Mt Bruno Elevation',
    width=400, height=400,
    margin=dict(t=40, r=0, l=20, b=20)
)

name = 'default'
# Default parameters which are used when `layout.scene.camera` is not provided
camera = dict(
    up=dict(x=0, y=0, z=1),
    center=dict(x=0, y=0, z=0),
    eye=dict(x=1.25, y=1.25, z=1.25)
)

fig.update_layout(scene_camera=camera, title=name)
# fig.show()
plotly.offline.iplot(fig)