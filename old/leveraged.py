import matplotlib.pyplot as plt
import numpy as np


def daily_rebalancing_loss(standard_deviation: float) -> float:
    return np.sqrt(1 - standard_deviation * standard_deviation) - 1


def daily_rebalanced_expected_return(expected_return: float, standard_deviation: float) -> float:
    return expected_return * (1 + daily_rebalancing_loss(standard_deviation))


one_dimensional_space = np.linspace(0, 1, 100)

expected_returns, standard_deviations = np.meshgrid(one_dimensional_space, one_dimensional_space)
daily_rebalanced_expected_returns = daily_rebalanced_expected_return(expected_returns, standard_deviations)

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.plot_surface(expected_returns, standard_deviations, daily_rebalanced_expected_returns, cmap='viridis', edgecolor='none')

ax.set_xlabel("Expected returns")
ax.set_ylabel("Standard deviations")
ax.set_zlabel("Total returns")
ax.set_title('Daily rebalanced portfolio yearly return given CAGR and standard deviation')

ax.azim = 170
ax.elev = 25

plt.show()
