from datetime import datetime
from typing import Callable, Union

import pandas as pd

df = pd.DataFrame({
    'x': [0.120117, 0.117188, 0.119141, 0.116211, 0.119141],
    'y': [0.987305, 0.984375, 0.987305, 0.984375, 0.983398],
    'z': [0.116211, 0.122070, 0.119141, 0.120117, 0.118164]},
    index=[
        '2014-05-15 10:38',
        '2014-05-15 10:39',
        '2014-05-15 10:40',
        '2014-05-15 10:41',
        '2014-05-15 10:42'],
    columns=['x', 'y', 'z'])


def myfunc(it):
    print(type(it))
    # do something
    return 7, 8, 9


# df['e'], df['f'], df['g'] = df.apply(myfunc, axis=1, result_type='expand').T.values


# x = df.apply(myfunc, axis=1, result_type='expand')
# print(x)

def calculate(data_frame: pd.DataFrame, new_column_name: str, function: Callable[[pd.Series], Union[float, int, datetime, str]]) -> pd.DataFrame:
    data_frame[new_column_name] = data_frame.apply(function, axis=1).T.values

    return data_frame


df = calculate(df, 'asd', lambda row: 7)

print(df)
